@extends('layouts.app')

@section('page-name','Dashboard')
 

@section('content')
    <div class="page-header">
        <h1 class="page-title">
            Dashboard
        </h1>
    </div>
    <div class="row">
        <div class="col-4">
            <a href="{{ route('spp.index') }}" class="nav-link {{ set_active(['web.*'], 'active') }}"> 
                <div class="card">
                    <div class="card-body p-3 text-center">
                        <div class="h1 m-0"><i class="fe fe-repeat"></i> </div> 
                    <div class="text-muted mb-4">Transaksi</div>

                    </div>
                </div>
            </a>
        </div>
        <div class="col-4">
            <a href="{{ route('tagihan.index') }}" class="nav-link {{ set_active(['web.*'], 'active') }}"> 
                <div class="card">
                    <div class="card-body p-3 text-center">
                        <div class="h1 m-0"><i class="fe fe-box"></i>  </div> 
                    <div class="text-muted mb-4">Tagihan</div>

                    </div>
                </div>
            </a>
        </div><div class="col-4">
            <a href="{{ route('siswa.index') }}" class="nav-link {{ set_active(['web.*'], 'active') }}"> 
                <div class="card">
                    <div class="card-body p-3 text-center">
                        <div class="h1 m-0"><i class="fe fe-users"></i> </div> 
                    <div class="text-muted mb-4">Siswa</div>

                    </div>
                </div>
            </a>
        </div>
        <div class="col-4" style="margin-left:17%">
            <a href="{{ route('kelas.index') }}" class="nav-link {{ set_active(['web.*'], 'active') }}"> 
                <div class="card">
                    <div class="card-body p-3 text-center">
                        <div class="h1 m-0"><i class="fe fe-box"></i>  </div> 
                    <div class="text-muted mb-4">Kelas</div>

                    </div>
                </div>
            </a>
        </div>
        <div class="col-4">
            <a href="{{ route('periode.index') }}" class="nav-link {{ set_active(['web.*'], 'active') }}"> 
                <div class="card">
                    <div class="card-body p-3 text-center">
                        <div class="h1 m-0"><i class="fe fe-box"></i> </div> 
                    <div class="text-muted mb-4">Periode</div>

                    </div>
                </div>
            </a>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Laporan Harian : {{ now()->format('d-m-Y') }}</h3>
                    <div class="card-options">
                        <input class="form-control mr-2" type="text" name="dates" style="max-width: 200px" data-toggle="datepicker" autocomplete="off" value="{{ now()->format('d-m-Y') }}" id="date">
                        
                    </div>
                </div>
                <div class="card-body">
                    <table class="table card-table table-hover table-vcenter text-nowrap title" id="print">
                        <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Nama</th>
                            <th>Pembayaran</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($transaksi as $item)
                            <tr>
                                <td>{{ $item->created_at->format('d-m-Y') }}</td>
                                <td>{{ $item->siswa->nama }}</td>
                                <td>{{ $item->tagihan->nama }}</td>
                                <td>IDR. {{ format_idr($item->keuangan->jumlah) }}</td>
                                @php
                                    $jumlah += $item->keuangan->jumlah
                                @endphp
                            </tr>
                        @endforeach
                            <tr>
                                <td><b>Total</b></td>
                                <td></td>
                                <td></td>
                                <td>IDR. {{ format_idr($jumlah) }}</td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        require(['jquery', 'selectize','datepicker'], function ($, selectize) {
        $(document).ready(function () {
                $('[data-toggle="datepicker"]').datepicker({
                    format: 'dd-MM-yyyy'
                });
                $('#btn-cetak-spp').on('click', function(){
                    var form = document.createElement("form");
                    form.setAttribute("style", "display: none");
                    form.setAttribute("method", "post");
                    form.setAttribute("action", "{{route('laporan-harian.cetak')}}");
                    form.setAttribute("target", "_blank");
                    
                    var token = document.createElement("input");
                    token.setAttribute("name", "_token");
                    token.setAttribute("value", "{{csrf_token()}}");
                    
                    var dateForm = document.createElement("input");
                    dateForm.setAttribute("name", "date");
                    dateForm.setAttribute("value", $('#date').val());

                    form.appendChild(token);
                    form.appendChild(dateForm);
                    document.body.appendChild(form);
                    form.submit();
                })
                $('#btn-export-spp').on('click', function(){
                    var form = document.createElement("form");
                    form.setAttribute("style", "display: none");
                    form.setAttribute("method", "post");
                    form.setAttribute("action", "{{route('laporan-harian.export')}}");
                    form.setAttribute("target", "_blank");
                    
                    var token = document.createElement("input");
                    token.setAttribute("name", "_token");
                    token.setAttribute("value", "{{csrf_token()}}");
                    
                    var dateForm = document.createElement("input");
                    dateForm.setAttribute("name", "date");
                    dateForm.setAttribute("value", $('#date').val());

                    form.appendChild(token);
                    form.appendChild(dateForm);
                    document.body.appendChild(form);
                    form.submit();
                })
            });
        });
    </script>
@endsection